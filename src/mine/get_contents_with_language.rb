language = ARGV[0]

#TODO: at the moment this gets a max of 100 files from each repo, I need to incorporate the "incomplete_results" from the returned data, but that means grabbing two things with jq, which I am not keen on just now

def chase(lang, user, repo)
	startTime = Time.now
	files = `curl -u #{ENV['GIT_USER']}:#{ENV['GIT_CRED']} -s 'https://api.github.com/search/code?q=language:#{lang}+repo:#{user}/#{repo}' | jq -r '.items | .[] | .url'`
	if (files.strip != "") 
		puts files
	end
	timeGap = startTime + 3 - Time.now # search api maxes out at 30 per minute, playing it safe at 20 per minute
	if (timeGap > 0)
		sleep(timeGap)
	end
end

File.open("mined/#{language}.txt").each do |repo|
	chase(language, repo[/mined\/repo_details\/(.*)_slash_.*/, 1], repo[/.*_slash_(.*)\.languages/, 1])
end


base_url = "https://api.github.com/repos"


File.open("repos.txt").each do |url|
	startTime = Time.now
	puts url

	parts = url.match /.*github.com\/(.*)\/(.*)/


	if (parts)
		url = "#{base_url}/#{parts[1]}/#{parts[2]}/languages"
	

		puts url
		system("curl -u #{ENV['GIT_USER']}:#{ENV['GIT_CRED']} -s '#{url}' > 'repo_details/#{parts[1]}_slash_#{parts[2]}.languages'")
	end
	timeGap = startTime + 1 - Time.now
	if (timeGap > 0)
		sleep(timeGap)
	end

end

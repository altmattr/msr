url = "https://archive.softwareheritage.org/api/1/origins/?origin_count=100&page_token=3567338"


if (ARGV[0])
	url = ARGV[0]
end

while (url)
	puts "getting batch of repos #{url}\n"
	urls = system("curl -s '#{url}' | jq -r '.[] | .url' >> repos.txt")
	system("sort -u -o repos.txt repos.txt")
	puts "getting next #{url}\n"
	headers = `curl -s -I '#{url}'`
	#puts headers
	time = headers[/X-RateLimit-Reset: (.*)/, 1]
	remaining = headers[/X-RateLimit-Remaining: (.*)/, 1]
	puts "remaining #{remaining}"
	timenow = `date +%s`
	puts "reset in #{time.to_i - timenow.to_i}"
	if (remaining.to_i == 0)
		timetosleep = (time.to_i - timenow.to_i) + 60
		puts "sleeping for #{timetosleep}"
		sleep([timetosleep, 0].max)
	else
		url = headers[/Link: <(.*)>/, 1]
	end
end
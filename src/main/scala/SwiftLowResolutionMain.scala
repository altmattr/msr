import org.bitbucket.inkytonik.kiama.util.{CompilerBase, Config}
import org.bitbucket.inkytonik.kiama.util.Messaging.Messages
import org.bitbucket.inkytonik.kiama.output.PrettyPrinterTypes.Document
import org.bitbucket.inkytonik.kiama.rewriting.Rewriter._
import org.bitbucket.inkytonik.kiama.util._
import parsers.SwiftLowResolutionSyntax._
import parsers.SwiftLowResolutionPrettyPrinter
import parsers.SwiftLowResolution
import sys.process._
import java.time._

object SwiftLowResolutionMain extends AnalyseFromSourceList[ASTNode, Program] {

  val name        = "swift switches (low res)"
  val cacheLoc    = "mined/swift_sources/"
  val sourceList  = "mined/swift_sources.txt"

  override def makeast(source: Source, config: Config) : Either[Program, Messages] = {
    val p = new SwiftLowResolution(source, positions)
    val pr = p.pProgram(0)
    if (pr.hasValue){
      Left(p.value(pr).asInstanceOf[Program])
    } else {
      Right(Vector(p.errorToMessage(pr.parseError)))
    }
  }

  override def format(prog : Program) : Document =
    SwiftLowResolutionPrettyPrinter.format(prog, 5)

  override def main(args: Array[String]): Unit = {
    var finds = List[String]()
    
    onAllSources {filename =>
      val ast = makeast(new FileSource(filename), createConfig(List()))

      ast match {
        case Left(prog) => {(everywhere (queryf {
                      case SwitchExp(disc, rest) => finds = (s"${filename}\n -- switch on ${disc}")::finds
                      case _ => {}
                      }
                   ))(prog)
        }
        case Right(messages) => System.out.println(s"PARSE ERROR : $filename")
      } //ast match
    }
    finds.foreach(s => System.out.println(s))

  }

}


import org.bitbucket.inkytonik.kiama.util.{CompilerBase, Config}
import org.bitbucket.inkytonik.kiama.util.Messaging.Messages
import org.bitbucket.inkytonik.kiama.output.PrettyPrinterTypes.Document
import org.bitbucket.inkytonik.kiama.rewriting.Rewriter._
import org.bitbucket.inkytonik.kiama.util._
import sys.process._
import java.time._

trait AnalyseFromSourceList[ASTNode, Program <: ASTNode] extends CompilerBase[ASTNode, Program, Config] {

  val name      :String
  val cacheLoc  :String
  val sourceList:String
  
  if(!sys.env.contains("GIT_USER") || !sys.env.contains("GIT_CRED")){
    System.out.println("you must define GIT_USER and GIT_CRED")
    System.exit(1)
  }
  val git_user = sys.env("GIT_USER")
  val git_cred = sys.env("GIT_CRED")

  def createConfig(args : Seq[String]) : Config =
    new Config(args)

  def process(source: Source, ast: Program, config: Config): Unit =
    System.out.println("process??")

  def onAllSources(action: String => Unit): Unit = {
    val source = scala.io.Source.fromFile(sourceList)
    
    for (line <- source.getLines()) {
      // do we have this file?
      if(!java.nio.file.Files.exists(java.nio.file.Paths.get(cacheLoc + urlToFileName(line)))){
        val start = Instant.now()
        val command = (s"curl -u ${git_user}:${git_cred} -s " + line)
        println(command)
        val text = command.!!
        val pattern = """\"download_url\":.\"(.*)\"""".r.unanchored
        text match {
          case pattern(url) => { try {
                                   val text = ((s"""curl -u ${git_user}:${git_cred} -s "${url}"""") #| ("tee " + cacheLoc+ urlToFileName(line))).!!
                                 } catch {
                                   case e:Throwable => println("error trying to grab file (curl) " + line); e.printStackTrace();
                                 }
                               }
          case _            => { println("error trying to grab filei (trying to find url) " + line)
                                 println(text)
                               }
        }
        val end = Instant.now()
        // slow down if needed
        if (end.isBefore(start.plusSeconds(1)))
          Thread.sleep(1000)
      } //if

      // if we managed to get the file, run the action
      if(java.nio.file.Files.exists(java.nio.file.Paths.get(cacheLoc + urlToFileName(line)))){
        action(cacheLoc + urlToFileName(line))
      }

    }
    source.close()
  }
  
  def urlToFileName(url: String): String = {
    url.replace("https://", "").replace("/", "_")
  }
}


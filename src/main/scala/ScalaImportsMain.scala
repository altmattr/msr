import org.bitbucket.inkytonik.kiama.util.{CompilerBase, Config}
import org.bitbucket.inkytonik.kiama.util.Messaging.Messages
import org.bitbucket.inkytonik.kiama.output.PrettyPrinterTypes.Document
import org.bitbucket.inkytonik.kiama.rewriting.Rewriter._
import org.bitbucket.inkytonik.kiama.util._
import parsers.ScalaImportsSyntax._
import parsers.ScalaImportsPrettyPrinter
import parsers.ScalaImports
import sys.process._
import java.time._

object ScalaImportsMain extends AnalyseFromSourceList[ASTNode, Program] {

  val name        = "scala imports"
  val cacheLoc    = "mined/scala_sources/"
  val sourceList  = "mined/scala_sources.txt"

  override def makeast(source: Source, config: Config) : Either[Program, Messages] = {
    val p = new ScalaImports(source, positions)
    val pr = p.pProgram(0)
    if (pr.hasValue){
      Left(p.value(pr).asInstanceOf[Program])
    } else {
      Right(Vector(p.errorToMessage(pr.parseError)))
    }
  }

  override def format(prog : Program) : Document =
    ScalaImportsPrettyPrinter.format(prog, 5)

  override def main(args: Array[String]): Unit = {
    var finds = List[Import]()
    
    onAllSources {filename =>
      val ast = makeast(new FileSource(filename), createConfig(List()))

      ast match {
        case Left(prog) => {(everywhere (queryf {
                      case s: Import => finds = s::finds
                      case _ => {}
                      }
                   ))(prog)
        }
        case Right(messages) => System.out.println(s"PARSE ERROR : $filename")
      } //ast match
    }
    
    consolidate(finds, scala.collection.immutable.Map[Import, Int]()).toSeq.sortBy(_._2).reverse.foreach { case (Import(k), v) => {
      println(s"$v: $k")
    }}

  }

  def consolidate[A](lst: List[A], accum: scala.collection.immutable.Map[A, Int]) : scala.collection.immutable.Map[A, Int] = {
    lst match {
      case x :: xs => accum.get(x) match {
        case None    => consolidate(xs, accum + ((x -> 1)))
        case Some(y) => consolidate(xs, accum.updated(x, y+1))
        }
      case Nil => accum
    }
  }
}


import org.bitbucket.inkytonik.kiama.util.{CompilerBase, Config}
import org.bitbucket.inkytonik.kiama.util.Messaging.Messages
import org.bitbucket.inkytonik.kiama.output.PrettyPrinterTypes.Document
import org.bitbucket.inkytonik.kiama.rewriting.Rewriter._
import org.bitbucket.inkytonik.kiama.util._
import parsers.JavaImportsSyntax._
import parsers.JavaImportsPrettyPrinter
import parsers.JavaImports

object JavaImportsMain extends AnalyseFromSourceList[ASTNode, Program] {

  val name        = "java imports"
  val cacheLoc    = "mined/java_sources/"
  val sourceList  = "mined/java_sources.txt"

  override def makeast(source: Source, config: Config) : Either[Program, Messages] = {
    val p = new JavaImports(source, positions)
    val pr = p.pProgram(0)
    if (pr.hasValue){
      Left(p.value(pr).asInstanceOf[Program])
    } else {
      Right(Vector(p.errorToMessage(pr.parseError)))
    }
  }

  override def format(prog : Program) : Document =
    JavaImportsPrettyPrinter.format(prog, 5)

  override def main(args: Array[String]): Unit = {
    var finds = List[Import]()
    
    onAllSources {filename =>
      println(filename)
      val ast = makeast(new FileSource(filename), createConfig(List()))

      ast match {
        case Left(prog) => {(everywhere (queryf {
                      case s: Import => finds = s::finds
                      case _ => {}
                      }
                   ))(prog)
        }
        case Right(messages) => System.out.println(s"PARSE ERROR : $filename")
      } //ast match
      consolidate(finds, scala.collection.immutable.Map[Import, Int]()).toSeq.sortBy(_._2).reverse.foreach { case (i@Import(idents, rest), v) => {
        println(s"$v: ${fullPackage(idents)}")
      }}
    }
   
    println("\n----- Final Results -----")
    consolidate(finds, scala.collection.immutable.Map[Import, Int]()).toSeq.sortBy(_._2).reverse.foreach { case (Import(idents, rest), v) => {
      println(s"$v: $idents")
    }}

  }

  def fullPackage(i: Vector[String]) : String = i match {
    case i +: idents => i ++ "." ++ fullPackage(idents)
    case _           => ""
  }

  def consolidate[A](lst: List[A], accum: scala.collection.immutable.Map[A, Int]) : scala.collection.immutable.Map[A, Int] = {
    lst match {
      case x :: xs => accum.get(x) match {
        case None    => consolidate(xs, accum + ((x -> 1)))
        case Some(y) => consolidate(xs, accum.updated(x, y+1))
        }
      case Nil => accum
    }
  }
}


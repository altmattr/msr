name := "msr"
version := "0.1.0"
scalaVersion in ThisBuild := "2.13.1"

lazy val root = (project in file("."))
	.settings(
		scalaVersion := "2.13.1",
		name := "island grammars for msr",
		version := "0.0.1",
		resolvers ++= Seq (
			Resolver.sonatypeRepo ("releases"),
			Resolver.sonatypeRepo ("snapshots")
		),
		mainClass in Compile := Some("Main"),
		libraryDependencies ++= Seq(
			"org.bitbucket.inkytonik.kiama" %% "kiama" % "2.3.0",
			"org.bitbucket.inkytonik.kiama" %% "kiama" % "2.3.0" % "test" classifier ("tests"),
			"org.bitbucket.inkytonik.kiama" %% "kiama-extras" % "2.3.0",
			"org.bitbucket.inkytonik.kiama" %% "kiama-extras" % "2.3.0" % "test" classifier ("tests")
		),
		// sbt-rats
		ratsScalaRepetitionType := Some(VectorType),
		ratsUseScalaOptions := true,
		ratsUseScalaPositions := true,
		ratsDefineASTClasses := true,
		ratsDefinePrettyPrinter := true,
		ratsUseKiama := 2, // 2 to turn on
	)


# Setup

Everything that follows relies on you having credentials to access the github API.  These should be set in environment variables `GIT_USER` and `GIT_CRED`

For example

~~~~~
export GIT_USER=whoiam
export GIT_CRED=j8odpfa7gfd89gsdf7g8sdf078g09dfs
~~~~~

# Finding open source files for analysing language use patterns

This repository helps to find examples of certain languages from openly available software on the web.  The pipeline is

~~~~~
PHASE 1                                      ------------> PHASE 2                                  ----> PHASE 3
search softwareheritage for repository names -- github --> filter repos based on reported languages ----> pull files of the interesting language from these repos
                                             -- other  --> TODO
~~~~~

## Phase 1

Is done by the `get_repos.rb` script, which populates the file `repos.txt`.  `repos.txt` is getting very big, but it is all within the capabilities of a desktop machine, no worries.  The first line of the script has the URL to start at, note the existance of a token telling it where to start the search on `softwareheritage.org`.  If you want to start again, remove the URL parameters from here but you will be adding already existing repos into the text file.  Since softwareheritage updates while we are "away", we might miss some by starting at the last ending point, but we are not trying to be exhaustive

## Phase 2 (github)

Is done by the `get_langauges_github.rb` which uses the github API to get the languages reported for that repository.  We are entirely dependent on the (not completely accurate) language detection in github.  Each repostitory explored gets a file in `repo_details/` telling us what languages github thinks it contains.  

_You need to re-run this script every time you get new data from Phase 1_

The `get_large_repos_of_langauges.rb`, when passed a language name, will summarise this information for a particular language and will put the results into the file `language.txt`.  It will report all languages that have (according to github) at least 10000 bytes of code in that language.  We add this lower limit to try and avoid collecting toy code.

## Phase 3 (github)

Is done by `get_contents_with_language.rb` which, when passed a language name, will use the github seach API to find _the first 100 files_ (TODO: need to fix this) of that language in each identified repository.  The results go on STDOUT, so redirect them somewhere (like `language_sources.txt`).

# Analysing the source files found

Now you can generate a list of open-source files for any partilar language, you might like to analyse them.  This project contains example scala code that uses SBT Rats! and Kiama for easy analysis of mined source code.  `AnalyseFromSourceList.scala` is a trait that any `main` object can extend to get the basic functionality.  It contains an `onAllSources` method that takes a function and applies that function on every source file it finds (based on the file you set).  Take a look in `ScalaImportsMain.scala` for an example of how to use this.  Note the use of `query` and `everywhere` from Kiama to easily search arbitrary abstract syntax trees for particular nodes.
